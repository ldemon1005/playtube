<?php 
// +------------------------------------------------------------------------+
// | @author Deen Doughouz (DoughouzForest)
// | @author_url 1: http://www.playtubescript.com
// | @author_url 2: http://codecanyon.net/user/doughouzforest
// | @author_email: wowondersocial@gmail.com   
// +------------------------------------------------------------------------+
// | PlayTube - The Ultimate Video Sharing Platform
// | Copyright (c) 2017 PlayTube. All rights reserved.
// +------------------------------------------------------------------------+
define('T_USERS', 'users');
define('T_SESSIONS', 'sessions');
define('T_VIDEOS', 'videos');
define('T_DIS_LIKES', 'likes_dislikes');
define('T_COMMENTS', 'comments');
define('T_COMMENTS_LIKES', 'comments_likes');
define('T_SAVED', 'saved_videos');
define('T_SUBSCRIPTIONS', 'subscriptions');
define('T_HISTORY', 'history');
define('T_CONFIG', 'config');
define('T_VIDEO_ADS', 'video_ads');
define('T_ADS', 'site_ads');
define('T_TERMS', 'terms');
define('T_LISTS', 'lists');
define('T_PLAYLISTS', 'play_list');
?>