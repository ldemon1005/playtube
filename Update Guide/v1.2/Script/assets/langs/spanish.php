<?php 
$lang_array      = array(
    'login' => 'Iniciar sesión',
	'search_keyword' => 'Buscar videos',
	'register' => 'Registro',
	'invalid_request' => 'Solicitud no válida',
	'please_check_details' => 'Por favor verifique los detalles',
	'email_sent' => 'E-mail enviado correctamente',
	'email_not_exist' => 'E-mail no existe',
	'reset_password' => 'Restablecer la contraseña',
	'account_is_not_active' => 'Su cuenta aún no está activa, por favor confirme su E-mail.',
	'resend_email' => 'Reenviar email',
	'invalid_username_or_password' => 'usuario o contraseña invalido',
	'gender' => 'Género',
	'gender_is_invalid' => 'Género no válido',
	'country' => 'País',
	'username_is_taken' => 'El nombre de usuario se ha tomado',
	'username_characters_length' => 'El nombre de usuario debe estar entre 5/32',
	'username_invalid_characters' => 'Caracteres de usuario no válidos',
	'email_exists' => 'Este correo electrónico ya está en uso',
	'email_invalid_characters' => 'El correo electrónico es invalido',
	'password_not_match' => 'La contraseña no coincide',
	'password_is_short' => 'La contraseña es demasiado corta',
	'reCaptcha_error' => 'Por favor, compruebe la re-captcha.',
	'successfully_joined_desc' => '¡Registro exitoso! Te hemos enviado un correo electrónico, verifica tu bandeja de entrada / spam para verificar tu cuenta.',
	'change_password' => 'Cambia la contraseña',
	'404_desc' => 'La página que buscabas no existe.',
	'404_title' => '404 Pagina no encontrada',
	'your_email_address' => 'Tu correo electrónico',
	'request_new_password' => 'Pide nueva contraseña',
	'got_your_password' => '¿Tienes tu contraseña?',
	'login_' => 'Iniciar sesión',
	'please_wait' => 'Por favor espera..',
	'welcome_back' => '¡Dar una buena acogida!',
	'username' => 'Nombre de usuario',
	'password' => 'Contraseña',
	'forgot_your_password' => '¿Olvidaste tu contraseña?',
	'sign_up' => '¡Regístrate!',
	'new_here' => '¿Nuevo aquí?',
	'lets_get_started' => '¡Empecemos!',
	'email_address' => 'Dirección de correo electrónico',
	'confirm_password' => 'Confirmar contraseña',
	'male' => 'Masculino',
	'female' => 'Hembra',
	'already_have_account' => '¿Ya tienes una cuenta?',
	'home' => 'Casa',
	'upload' => 'Subir',
	'terms_of_use' => 'Términos de Uso',
	'privacy_policy' => 'Política de privacidad',
	'about_us' => 'Sobre nosotros',
	'language' => 'Idioma',
	'copyright' => 'Copyright © {{DATE}} {{CONFIG name}}. Todos los derechos reservados.',
	'profile' => 'Perfil',
	'edit' => 'Editar',
	'settings' => 'Ajustes',
	'log_out' => 'Cerrar sesión',
	'featured_video' => 'Vídeo destacado',
	'subscribe' => 'Suscribir',
	'views' => 'puntos de vista',
	'save' => 'Salvar',
	'share' => 'Compartir',
	'embed' => 'Empotrar',
	'report' => 'Informe',
	'published_on' => 'Publicado en',
	'in' => 'En',
	'top_videos' => 'Los mejores videos',
	'trending' => 'Tendencias',
	'explore_more' => 'Explora más',
	'year' => 'año',
	'month' => 'mes',
	'day' => 'día',
	'hour' => 'hora',
	'minute' => 'minuto',
	'second' => 'segundo',
	'years' => 'años',
	'months' => 'meses',
	'days' => 'días',
	'hours' => 'horas',
	'minutes' => 'minutos',
	'seconds' => 'segundos',
	'time_ago' => 'hace',
	'url_not_supported' => 'URL no es compatible.',
	'no_more_comments' => 'No se encontraron comentarios',
	'video_not_found_please_try_again' => 'No se encontró el video, actualice la página e inténtelo de nuevo.',
	'saved' => 'Salvado',
	'no_comments_found' => 'No se encontraron comentarios',
	'import' => 'Importar',
	'import_new_video' => 'Importar nuevo video',
	'video_url' => 'URL del vídeo',
	'url_desc' => 'URL de YouTube, Dailymotion, Vimeo',
	'fetch_Video' => 'Obtener video',
	'video_title' => 'Titulo del Video',
	'video_title_help' => 'Su título de vídeo, 2 - 55 caracteres',
	'video_descritpion' => 'Descripción del video',
	'category' => 'Categoría',
	'tags' => 'Etiquetas',
	'tags_help' => 'Etiquetas, seprated by comma',
	'publish' => 'Publicar',
	'upload_new_video' => 'Subir nuevo video',
	'choose_new_file' => 'Elige un archivo de video ..',
	'thumbnail' => 'Miniatura',
	'successfully_uplaoded' => 'cargado con éxito.',
	'reply' => 'Respuesta',
	'show_more' => 'Mostrar más',
	'comments' => 'Comentarios',
	'write_your_comment' => 'Escriba su comentario ..',
	'fb_comments' => 'Comentarios de Facebook',
	'related_videos' => 'Videos relacionados',
	'delete_confirmation' => '¿Seguro que quieres eliminar tu comentario?',
	'subscribed' => 'Suscrito',
	'no_videos_found_subs' => 'No videos encontrados, suscríbase para empezar!',
	'subscriptions' => 'Suscripciones',
	'no_videos_found_history' => 'No se han encontrado vídeos, ¡mira para empezar!',
	'history' => 'Historia',
	'no_videos_found_liked' => 'No se han encontrado vídeos, ¡como para empezar!',
	'liked_videos' => 'Videos que me gustaron',
	'latest_videos' => 'Últimos vidéos',
	'no_videos_found_for_now' => 'No se encontraron videos por ahora!',
	'no_more_videos_to_show' => 'No hay más videos para mostrar',
	'categories' => 'Categorías',
	'video_already_exist' => 'El video ya existe',
	'video_saved' => 'Video actualizado correctamente',
	'manage_videos' => 'Administrar vídeos',
	'search' => 'Buscar',
	'manage' => 'Gestionar',
	'edit_video' => 'Editar video',
	'delete_video_confirmation' => '¿Seguro que quieres eliminar este video? No se puede deshacer esta acción',
	'manage_my_videos' => 'Administrar mis videos',
	'delete_videos' => 'Eliminar vídeo',
	'search_results' => 'Resultados de la búsqueda',
	'status' => 'Estado',
	'active' => 'Activo',
	'inactive' => 'Inactivo',
	'type' => 'Tipo',
	'user' => 'Usuario',
	'admin' => 'Administración',
	'verification' => 'Verificación',
	'verified' => 'Verificado',
	'not_verified' => 'No verificado',
	'setting_updated' => 'Configuración actualizada con éxito!',
	'first_name' => 'Nombre de pila',
	'last_name' => 'Apellido',
	'about_profile' => 'Acerca de',
	'facebook' => 'Facebook',
	'google_plus' => 'Google+',
	'twitter' => 'Gorjeo',
	'current_password' => 'Passowrd actual',
	'new_password' => 'Nuevo pasatiempo',
	'confirm_new_password' => 'Confirmar nueva contraseña',
	'current_password_dont_match' => 'La contraseña actual no coincide.',
	'new_password_dont_match' => 'La nueva contraseña no coincide.',
	'avatar' => 'Avatar',
	'cover' => 'Cubrir',
	'your_account_was_deleted' => 'Se ha eliminado tu cuenta.',
	'avatar_and_cover' => 'Avatar y portada',
	'general' => 'General',
	'delete_account' => 'Borrar cuenta',
	'general_settings' => 'Configuración general',
	'password_settings' => 'Configuración de la contraseña',
	'profile_settings' => 'Configuración de perfil',
	'videos' => 'Videos',
	'up_next' => 'Hasta la próxima',
	'autoplay' => 'Auto reproducción',
	'featured' => 'Destacados',
	'saved_videos' => 'Videos guardados',
	'my_channel' => 'Mi canal',
    'add_to' => 'Agregar a',
    'add_to_pl' => 'Agregar a lista de reproducción',
    'create_new' => 'Crear nuevo',
    'close' => 'cerca',
    'removed_from' => 'Eliminado de',
    'added_to' => 'Agregado a',
    'create_new_pl' => 'Crear nueva lista de reproducción',
    'pl_name' => 'Nombre de la lista de reproducción',
    'privacy' => 'intimidad',
    'description' => 'descripción',
    'create' => 'crear',
    'cancel' => 'cancelar',
    'pl_name_required' => 'Se requiere el nombre de la lista de reproducción.',
    'play_lists' => 'las listas de reproducción',
    'delete' => 'borrar',
    'confirmation' => 'Confirmación!',
    'confirm_delist' => '¿Estás seguro de que quieres eliminar esta PlayList?',
    'yes_del' => 'Sí, ¡bórralo!',
    'deleted' => 'Borrado!',
    'was_deleted' => 'ha sido eliminado',
    'no_lists_found' => 'No se encontraron listas de reproducción',
    'public' => 'público',
    'private' => 'privado',
    'сreated' => 'creado',
    'pl_сreated' => '¡La lista de reproducción se agregó con éxito!',
    'saved' => 'guardado',
    'pl_saved' => '¡La lista de reproducción se guardó con éxito!',
    'watch_later' => 'Ver despues',
    'articles' => 'Artículos',
    'search_articles' => 'Buscar artículos',
    'most_popular' => 'Más popular',
    'no_result_for' => 'Lo sentimos, no se encontraron resultados para',
    'no_post_found' => '¡No se han encontrado publicaciones!',
    'related_articles' => 'Artículos relacionados',
    'related_videos' => 'Videos relacionados',
    'share_to' => 'Compartir a',
    'no_more_articles' => 'No más artículos',
    'go_pro' => 'Hazte profesional',
    'buy_pro_pkg' => 'Descubre más funciones con el {{SITE_NAME}} paquete Pro!',
    'free_mbr' => 'miembro gratuito',
    'pro_mbr' => 'Pro miembro',
    'upload_1gb_limit' => 'Carga hasta 1GB de límite',
    'ads_will_show_up' => 'Los anuncios de videos aparecerán',
    'not_featured_videos' => 'Videos no destacados',
    'no_verified_badge' => 'Sin insignia verificada',
    'stay_free' => 'Mantente Libre',
    'upgrade' => 'Mejorar',
    'upload_1tr_limit' => 'Carga hasta 1000GB',
    'ads_wont_show_up' => 'No se mostrarán anuncios',
    'ur_are_featured' => 'Tus videos son presentados',
    'verified_badge' => 'Insignia verificada',
    'congratulations' => '¡Felicitaciones!',
    'uare_pro' => '¡Ha actualizado su perfil con éxito a usuario PRO!',
    'start_features' => 'Comienza a navegar por nuevas funciones',
    'import_limit_reached_upgrade' => '¡Has alcanzado tu límite de importación, actualízate a pro para importar videos ilimitados!',
    'upload_limit_reached_upgrade' => 'Has alcanzado el límite de carga y actualízate a profesional para subir videos ilimitados.',
    'import_limit_reached' => 'Has alcanzado tu límite de importación',
    'upload_limit_reached' => 'Has alcanzado tu límite de carga',
    'upgrade_now' => '¿Actualizar ahora?',
    'error' => '¡Error!',
    'error_msg' => 'Algo salió mal. ¡Inténtalo de nuevo más tarde!',
    'oops' => 'Uy',
    'reply' => 'Respuesta',
    'write_a_reply' => 'Escribe un comentario y presiona ENTER',
    'file_is_too_big' => 'El archivo es demasiado grande, el tamaño máximo de carga es',
    'ads' => 'publicidad',
    'wallet' => 'billetera',
    'status' => 'estatus',
    'category' => 'categoría',
    'name' => 'nombre',
    'results' => 'resultados',
    'spent' => 'gastado',
    'action' => 'acción',
    'views' => 'vistas',
    'clicks' => 'clics',
    'create_ad' => 'Crear anuncio',
    'my_balance' => 'MI EQUILIBRIO',
    'replenish_my_balance' => 'Reponer mi saldo',
    'amount' => 'cantidad',
    'replenish' => 'reponer',
    'create_new_ad' => 'Crear nuevo anuncio',
    'title' => 'título',
    'description' => 'descripción',
    'select_media' => 'Seleccionar medios',
    'pricing' => 'la fijación de precios',
    'placement' => 'colocación',
    'traget_audience' => 'Audiencia objetivo',
    'video_ad' => 'Videos (Formato de video / imagen)',
    'page_ad' => 'Barra lateral (Imagen de formato)',
    'cost_click' => 'Pago por clic',
    'cost_view' => 'Pago por impresión',
    'invalid_name' => 'El nombre debe estar entre 5/32',
    'invalid_url' => 'La URL no es válida. Por favor ingrese una URL válida',
    'invalid_ad_title' => 'El título del anuncio debe estar entre 5/100',
    'invalid_videoad_media' => 'El archivo multimedia no es válido. Seleccione una imagen / video válido',
    'invalid_pagead_media' => 'El archivo multimedia no es válido. Seleccione una imagen válida',
    'edit_ad' => 'Editar anuncio',
    'by' => 'por',
    'more_info' => 'Más información',
    'monetization' => 'monetización',
    'monetization_settings' => 'Configuración de monetización',
    'withdrawals' => 'Los retiros',
    'balance' => 'Saldo disponible',
    'min' => 'min',
    'submit_withdrawal_request' => 'Enviar solicitud',
    'cant_request_withdrawal' => 'No puede enviar la solicitud hasta que la solicitud anterior haya sido aprobada / rechazada',
    'withdrawal_request_amount_is' => 'Su saldo es {{BALANCE}}, la solicitud mínima de retiro es 50:',
    'min_withdrawal_request_amount_is' => 'La solicitud mínima de retiro es 50:',
    'withdrawal_request_sent' => '¡Su solicitud de retiro ha sido enviada con éxito!',
    'enabled' => 'Activado',
    'disabled' => 'discapacitado',
    'withdrawals_history' => 'Historia de Retiros',
    'rejected' => 'rechazado',
    'accepted' => 'aceptado',
    'requested_at' => 'Solicitado en',
    'confirm_delete_ad' => '¿Seguro que quieres eliminar este anuncio?',
    'ad_published' => 'Su anuncio ha sido publicado con éxito',
    'ad_saved' => 'Sus cambios en el anuncio se guardaron con éxito',
    'pending' => 'pendiente',
    'balance_is_0' => 'Su saldo de cartera actual es: 0, por favor, recargue su cartera para continuar.',
    'top_up' => 'Completar',
    'earn_mon' => 'Gane ({{CONFIG pub_price}} {{CONFIG payment_currency}}) por cada clic publicitario que obtenga de sus videos.'
);

$categories = array(
    '1' => 'Cine y Animación',
'2' => 'Automóviles y Vehículos',
'3' => 'Música',
'4' => 'Animales de compañía',
'5' => 'Deportes',
'6' => 'Viajes y Eventos',
'7' => 'Juego de azar',
'8' => 'Personas y blogs',
'9' => 'Comedia',
'10' => 'Entretenimiento',
'11' => 'Noticias y Política',
'12' => 'Cómo hacer y estilo',
'13' => 'Sin fines de lucro y activismo',
);

$countries_name   = array(
    '0' => 'Select Country',
    '1' => 'United States',
    '2' => 'Canada',
    '3' => 'Afghanistan',
    '4' => 'Albania',
    '5' => 'Algeria',
    '6' => 'American Samoa',
    '7' => 'Andorra',
    '8' => 'Angola',
    '9' => 'Anguilla',
    '10' => 'Antarctica',
    '11' => 'Antigua and/or Barbuda',
    '12' => 'Argentina',
    '13' => 'Armenia',
    '14' => 'Aruba',
    '15' => 'Australia',
    '16' => 'Austria',
    '17' => 'Azerbaijan',
    '18' => 'Bahamas',
    '19' => 'Bahrain',
    '20' => 'Bangladesh',
    '21' => 'Barbados',
    '22' => 'Belarus',
    '23' => 'Belgium',
    '24' => 'Belize',
    '25' => 'Benin',
    '26' => 'Bermuda',
    '27' => 'Bhutan',
    '28' => 'Bolivia',
    '29' => 'Bosnia and Herzegovina',
    '30' => 'Botswana',
    '31' => 'Bouvet Island',
    '32' => 'Brazil',
    '34' => 'Brunei Darussalam',
    '35' => 'Bulgaria',
    '36' => 'Burkina Faso',
    '37' => 'Burundi',
    '38' => 'Cambodia',
    '39' => 'Cameroon',
    '40' => 'Cape Verde',
    '41' => 'Cayman Islands',
    '42' => 'Central African Republic',
    '43' => 'Chad',
    '44' => 'Chile',
    '45' => 'China',
    '46' => 'Christmas Island',
    '47' => 'Cocos (Keeling) Islands',
    '48' => 'Colombia',
    '49' => 'Comoros',
    '50' => 'Congo',
    '51' => 'Cook Islands',
    '52' => 'Costa Rica',
    '53' => 'Croatia (Hrvatska)',
    '54' => 'Cuba',
    '55' => 'Cyprus',
    '56' => 'Czech Republic',
    '57' => 'Denmark',
    '58' => 'Djibouti',
    '59' => 'Dominica',
    '60' => 'Dominican Republic',
    '61' => 'East Timor',
    '62' => 'Ecuador',
    '63' => 'Egypt',
    '64' => 'El Salvador',
    '65' => 'Equatorial Guinea',
    '66' => 'Eritrea',
    '67' => 'Estonia',
    '68' => 'Ethiopia',
    '69' => 'Falkland Islands (Malvinas)',
    '70' => 'Faroe Islands',
    '71' => 'Fiji',
    '72' => 'Finland',
    '73' => 'France',
    '74' => 'France, Metropolitan',
    '75' => 'French Guiana',
    '76' => 'French Polynesia',
    '77' => 'French Southern Territories',
    '78' => 'Gabon',
    '79' => 'Gambia',
    '80' => 'Georgia',
    '81' => 'Germany',
    '82' => 'Ghana',
    '83' => 'Gibraltar',
    '84' => 'Greece',
    '85' => 'Greenland',
    '86' => 'Grenada',
    '87' => 'Guadeloupe',
    '88' => 'Guam',
    '89' => 'Guatemala',
    '90' => 'Guinea',
    '91' => 'Guinea-Bissau',
    '92' => 'Guyana',
    '93' => 'Haiti',
    '94' => 'Heard and Mc Donald Islands',
    '95' => 'Honduras',
    '96' => 'Hong Kong',
    '97' => 'Hungary',
    '98' => 'Iceland',
    '99' => 'India',
    '100' => 'Indonesia',
    '101' => 'Iran (Islamic Republic of)',
    '102' => 'Iraq',
    '103' => 'Ireland',
    '104' => 'Israel',
    '105' => 'Italy',
    '106' => 'Ivory Coast',
    '107' => 'Jamaica',
    '108' => 'Japan',
    '109' => 'Jordan',
    '110' => 'Kazakhstan',
    '111' => 'Kenya',
    '112' => 'Kiribati',
    '113' => 'Korea, Democratic People\'s Republic of',
    '114' => 'Korea, Republic of',
    '115' => 'Kosovo',
    '116' => 'Kuwait',
    '117' => 'Kyrgyzstan',
    '118' => 'Lao People\'s Democratic Republic',
    '119' => 'Latvia',
    '120' => 'Lebanon',
    '121' => 'Lesotho',
    '122' => 'Liberia',
    '123' => 'Libyan Arab Jamahiriya',
    '124' => 'Liechtenstein',
    '125' => 'Lithuania',
    '126' => 'Luxembourg',
    '127' => 'Macau',
    '128' => 'Macedonia',
    '129' => 'Madagascar',
    '130' => 'Malawi',
    '131' => 'Malaysia',
    '132' => 'Maldives',
    '133' => 'Mali',
    '134' => 'Malta',
    '135' => 'Marshall Islands',
    '136' => 'Martinique',
    '137' => 'Mauritania',
    '138' => 'Mauritius',
    '139' => 'Mayotte',
    '140' => 'Mexico',
    '141' => 'Micronesia, Federated States of',
    '142' => 'Moldova, Republic of',
    '143' => 'Monaco',
    '144' => 'Mongolia',
    '145' => 'Montenegro',
    '146' => 'Montserrat',
    '147' => 'Morocco',
    '148' => 'Mozambique',
    '149' => 'Myanmar',
    '150' => 'Namibia',
    '151' => 'Nauru',
    '152' => 'Nepal',
    '153' => 'Netherlands',
    '154' => 'Netherlands Antilles',
    '155' => 'New Caledonia',
    '156' => 'New Zealand',
    '157' => 'Nicaragua',
    '158' => 'Niger',
    '159' => 'Nigeria',
    '160' => 'Niue',
    '161' => 'Norfork Island',
    '162' => 'Northern Mariana Islands',
    '163' => 'Norway',
    '164' => 'Oman',
    '165' => 'Pakistan',
    '166' => 'Palau',
    '167' => 'Panama',
    '168' => 'Papua New Guinea',
    '169' => 'Paraguay',
    '170' => 'Peru',
    '171' => 'Philippines',
    '172' => 'Pitcairn',
    '173' => 'Poland',
    '174' => 'Portugal',
    '175' => 'Puerto Rico',
    '176' => 'Qatar',
    '177' => 'Reunion',
    '178' => 'Romania',
    '179' => 'Russian Federation',
    '180' => 'Rwanda',
    '181' => 'Saint Kitts and Nevis',
    '182' => 'Saint Lucia',
    '183' => 'Saint Vincent and the Grenadines',
    '184' => 'Samoa',
    '185' => 'San Marino',
    '186' => 'Sao Tome and Principe',
    '187' => 'Saudi Arabia',
    '188' => 'Senegal',
    '189' => 'Serbia',
    '190' => 'Seychelles',
    '191' => 'Sierra Leone',
    '192' => 'Singapore',
    '193' => 'Slovakia',
    '194' => 'Slovenia',
    '195' => 'Solomon Islands',
    '196' => 'Somalia',
    '197' => 'South Africa',
    '198' => 'South Georgia South Sandwich Islands',
    '199' => 'Spain',
    '200' => 'Sri Lanka',
    '201' => 'St. Helena',
    '202' => 'St. Pierre and Miquelon',
    '203' => 'Sudan',
    '204' => 'Suriname',
    '205' => 'Svalbarn and Jan Mayen Islands',
    '206' => 'Swaziland',
    '207' => 'Sweden',
    '208' => 'Switzerland',
    '209' => 'Syrian Arab Republic',
    '210' => 'Taiwan',
    '211' => 'Tajikistan',
    '212' => 'Tanzania, United Republic of',
    '213' => 'Thailand',
    '214' => 'Togo',
    '215' => 'Tokelau',
    '216' => 'Tonga',
    '217' => 'Trinidad and Tobago',
    '218' => 'Tunisia',
    '219' => 'Turkey',
    '220' => 'Turkmenistan',
    '221' => 'Turks and Caicos Islands',
    '222' => 'Tuvalu',
    '223' => 'Uganda',
    '224' => 'Ukraine',
    '225' => 'United Arab Emirates',
    '226' => 'United Kingdom',
    '227' => 'United States minor outlying islands',
    '228' => 'Uruguay',
    '229' => 'Uzbekistan',
    '230' => 'Vanuatu',
    '231' => 'Vatican City State',
    '232' => 'Venezuela',
    '233' => 'Vietnam',
    '238' => 'Yemen',
    '239' => 'Yugoslavia',
    '240' => 'Zaire',
    '241' => 'Zambia',
    '242' => 'Zimbabwe'
);
?>