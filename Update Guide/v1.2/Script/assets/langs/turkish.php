<?php 
$lang_array      = array(
    'login' => 'Oturum aç',
    'search_keyword' => 'Videoları arayın',
    'register' => 'Kayıt olmak',
    'invalid_request' => 'Geçersiz istek',
    'please_check_details' => 'Lütfen ayrıntıları kontrol edin',
    'email_sent' => 'E-posta başarıyla gönderildi',
    'email_not_exist' => 'E-posta yok',
    'reset_password' => 'Şifreyi yenile',
    'account_is_not_active' => 'Hesabınız henüz aktif değil, lütfen e-postanızı onaylayın.',
    'resend_email' => 'Elektronik postayı tekrar gönder',
    'invalid_username_or_password' => 'Geçersiz kullanıcı adı veya şifre',
    'gender' => 'Cinsiyet',
    'gender_is_invalid' => 'Cinsiyet geçersiz',
    'country' => 'ülke',
    'username_is_taken' => 'kullanıcı adı alınmış',
    'username_characters_length' => 'Kullanıcı adı 5/32 arasında olmalıdır',
    'username_invalid_characters' => 'Geçersiz kullanıcı adı karakterleri',
    'email_exists' => 'Bu e-posta zaten kullanılıyor',
    'email_invalid_characters' => 'E-posta geçersiz',
    'password_not_match' => 'Şifre eşleşmiyor',
    'password_is_short' => 'Şifre çok kısa',
    'reCaptcha_error' => 'Lütfen yeniden captcha\'yı kontrol edin.',
    'successfully_joined_desc' => 'Kayıt başarılı! Size bir e-posta gönderdik. Hesabınızı doğrulamak için lütfen gelen kutunuzu / spam\'inizi kontrol edin.',
    'change_password' => 'Şifre değiştir',
    '404_desc' => 'Aradığınız sayfa mevcut değil.',
    '404_title' => '404 Sayfa Bulunamadı',
    'your_email_address' => 'E',
    'request_new_password' => 'yeni şifre isteği',
    'got_your_password' => 'Şifreniz var mı?',
    'login_' => 'Oturum aç',
    'please_wait' => 'Lütfen bekle..',
    'welcome_back' => 'Tekrar hoşgeldiniz!',
    'username' => 'Kullanıcı adı',
    'password' => 'Parola',
    'forgot_your_password' => 'Parolanızı mı unuttunuz?',
    'sign_up' => 'Kaydol!',
    'new_here' => 'Burada yeni?',
    'lets_get_started' => 'Başlayalım!',
    'email_address' => 'E',
    'confirm_password' => 'Şifreyi Onayla',
    'male' => 'Erkek',
    'female' => 'Kadın',
    'already_have_account' => 'Zaten hesabınız var mı?',
    'home' => 'Ev',
    'upload' => 'Yükleme',
    'terms_of_use' => 'Kullanım Şartları',
    'privacy_policy' => 'Gizlilik Politikası',
    'about_us' => 'Hakkımızda',
    'language' => 'Dil',
    'copyright' => 'Telif Hakkı © {{DATE}} {{CONFIG name}}. Her hakkı saklıdır.',
    'profile' => 'Profil',
    'edit' => 'Düzenleme',
    'settings' => 'Ayarlar',
    'log_out' => 'Çıkış Yap',
    'featured_video' => 'Öne çıkan video',
    'subscribe' => 'Abone ol',
    'views' => 'görünümler',
    'save' => 'Kayıt etmek',
    'share' => 'Pay',
    'embed' => 'gömmek',
    'report' => 'Rapor',
    'published_on' => 'yayınlandı',
    'in' => 'İçinde',
    'top_videos' => 'En iyi videolar',
    'trending' => 'Trend',
    'explore_more' => 'Daha fazla keşfedin',
    'year' => 'yıl',
    'month' => 'ay',
    'day' => 'gün',
    'hour' => 'saat',
    'minute' => 'dakika',
    'second' => 'ikinci',
    'years' => 'yıl',
    'months' => 'ay',
    'days' => 'günler',
    'hours' => 'saatler',
    'minutes' => 'dakika',
    'seconds' => 'saniye',
    'time_ago' => 'önce',
    'url_not_supported' => 'URL desteklenmiyor.',
    'no_more_comments' => 'Başka yorum bulunamadı',
    'video_not_found_please_try_again' => 'Video bulunamadı, lütfen sayfayı yenileyin ve tekrar deneyin.',
    'saved' => 'kaydedilmiş',
    'no_comments_found' => 'Hiçbir yorum bulunamadı',
    'import' => 'İthalat',
    'import_new_video' => 'Yeni video aktar',
    'video_url' => 'Video URL\'si',
    'url_desc' => 'YouTube, Dailymotion, Vimeo URL\'leri',
    'fetch_Video' => 'Video getir',
    'video_title' => 'video başlığı',
    'video_title_help' => 'Video başlığınız, 2 - 55 karakter',
    'video_descritpion' => 'Video Açıklaması',
    'category' => 'Kategori',
    'tags' => 'Etiketler',
    'tags_help' => 'Etiketler, virgülle ayrılmış',
    'publish' => 'yayınlamak',
    'upload_new_video' => 'Yeni video yükle',
    'choose_new_file' => 'Video dosyasını seçin ..',
    'thumbnail' => 'başparmak tırnağı',
    'successfully_uplaoded' => 'başarıyla yüklendi.',
    'reply' => 'cevap',
    'show_more' => 'Daha fazla göster',
    'comments' => 'Yorumlar',
    'write_your_comment' => 'Yorumunuzu yazın ..',
    'fb_comments' => 'Facebook Yorumları',
    'related_videos' => 'İlgili videolar',
    'delete_confirmation' => 'Yorumu silmek istediğinizden emin misiniz?',
    'subscribed' => 'Abone',
    'no_videos_found_subs' => 'Hiçbir video bulunamadı, başlamak için abone olun!',
    'subscriptions' => 'Abonelikler',
    'no_videos_found_history' => 'Hiçbir video bulunamadı, başlamak için izleyin!',
    'history' => 'tarih',
    'no_videos_found_liked' => 'Hiçbir video bulunamadı, başlamak gibi!',
    'liked_videos' => 'Beğenilen videolar',
    'latest_videos' => 'En yeni videolar',
    'no_videos_found_for_now' => 'Şuan için bir video bulunamadı!',
    'no_more_videos_to_show' => 'Gösterilecek daha fazla video yok',
    'categories' => 'Kategoriler',
    'video_already_exist' => 'Video zaten var',
    'video_saved' => 'Video başarıyla güncellendi',
    'manage_videos' => 'Videoları Yönet',
    'search' => 'Arama',
    'manage' => 'yönetme',
    'edit_video' => 'Videoyu düzenle',
    'delete_video_confirmation' => 'Bu videoyu silmek istediğinizden emin misiniz? Bu işlem geri alınamaz',
    'manage_my_videos' => 'Videolarımı Yönet',
    'delete_videos' => 'Videoyu sil',
    'search_results' => 'arama sonuçları',
    'status' => 'durum',
    'active' => 'Aktif',
    'inactive' => 'etkisiz',
    'type' => 'tip',
    'user' => 'kullanıcı',
    'admin' => 'yönetim',
    'verification' => 'Doğrulama',
    'verified' => 'Doğrulanmış',
    'not_verified' => 'Doğrulanmadı',
    'setting_updated' => 'Ayarlar başarıyla güncellendi!',
    'first_name' => 'İsim',
    'last_name' => 'Soyadı',
    'about_profile' => 'hakkında',
    'facebook' => 'Facebook',
    'google_plus' => 'Google+',
    'twitter' => 'heyecan',
    'current_password' => 'Geçerli Geçiş',
    'new_password' => 'Yeni Passowrd',
    'confirm_new_password' => 'Yeni şifreyi onayla',
    'current_password_dont_match' => 'Geçerli şifre uyuşmuyor.',
    'new_password_dont_match' => 'Yeni şifre uyuşmuyor.',
    'avatar' => 'Avatar',
    'cover' => 'kapak',
    'your_account_was_deleted' => 'Hesabınız silindi',
    'avatar_and_cover' => 'Avatar & Kapak',
    'general' => 'Genel',
    'delete_account' => 'Hesabı sil',
    'general_settings' => 'Genel Ayarlar',
    'password_settings' => 'Şifre Ayarları',
    'profile_settings' => 'Profil ayarları',
    'videos' => 'Videolar',
    'up_next' => 'Bir sonraki',
    'autoplay' => 'Otomatik oynatma',
    'featured' => 'Öne çıkan',
    'saved_videos' => 'Kaydedilen Videolar',
    'my_channel' => 'Benim kanalım',
    'add_to' => 'Ekle',
    'add_to_pl' => 'Çalma listesine ekle',
    'create_new' => 'Yeni oluştur',
    'close' => 'yakın',
    'removed_from' => 'Kaldırıldı',
    'added_to' => 'Eklendi',
    'create_new_pl' => 'Yeni çalma listesi oluştur',
    'pl_name' => 'Çalma listesi adı',
    'privacy' => 'gizlilik',
    'description' => 'tanım',
    'create' => 'oluşturmak',
    'cancel' => 'iptal',
    'pl_name_required' => 'Çalma listesi adı gerekiyor.',
    'play_lists' => 'çalma',
    'delete' => 'silmek',
    'confirmation' => 'Onay!',
    'confirm_delist' => 'Bu Oynatma Listesini silmek istediğinizden emin misiniz?',
    'yes_del' => 'Evet, sil şunu!',
    'deleted' => 'Silinen!',
    'was_deleted' => 'silindi!',
    'no_lists_found' => 'Hiç PlayList Bulunamadı!',
    'public' => 'kamu',
    'private' => 'özel',
    'сreated' => 'düzenlendi',
    'pl_сreated' => 'Çalma listesi başarıyla eklendi!',
    'saved' => 'kaydedilmiş',
    'pl_saved' => 'Çalma listesi başarıyla kaydedildi!',
    'watch_later' => 'Daha sonra izle',
    'articles' => 'Makaleler',
    'search_articles' => 'Makaleleri ara',
    'most_popular' => 'En popüler',
    'no_result_for' => 'Üzgünüz, bunun hakkında bir sonuç yok',
    'no_post_found' => 'Gönderi bulunamadı!',
    'related_articles' => 'İlgili Makaleler',
    'related_videos' => 'İlgili videolar',
    'share_to' => 'Paylaş',
    'no_more_articles' => 'Artık Makale Yok',
    'go_pro' => 'Yanlışa git',
    'buy_pro_pkg' => '{{SITE_NAME}} Pro paketi ile daha fazla özellik keşfedin!',
    'free_mbr' => 'Ücretsiz Üye',
    'pro_mbr' => 'Pro Üyesi',
    'upload_1gb_limit' => '1GB limitine kadar yükle',
    'ads_will_show_up' => 'Videolar reklamları gösterilir',
    'not_featured_videos' => 'Özellikli video yok',
    'no_verified_badge' => 'Doğrulanmış rozet yok',
    'stay_free' => 'Bedava Kal',
    'upgrade' => 'yükseltmek',
    'upload_1tr_limit' => '1000GB\'a kadar yükle',
    'ads_wont_show_up' => 'Hiçbir reklam gösterilmez',
    'ur_are_featured' => 'Videolarınız öne çıkıyor',
    'verified_badge' => 'Doğrulanmış rozet',
    'congratulations' => 'Tebrik ederiz!',
    'uare_pro' => 'Profilinizi PRO kullanıcılarına başarıyla yükselttiniz!',
    'start_features' => 'Yeni özelliklere göz atmaya başlayın',
    'import_limit_reached_upgrade' => 'İçe aktarma limitinize ulaştınız, sınırsız videoları içe aktarmak için yanlızca yükseltin!',
    'upload_limit_reached_upgrade' => 'Yükleme limitinize ulaştınız, sınırsız video yüklemek için profesyonelliğe geçin!',
    'import_limit_reached' => 'İçe aktarma limitinize ulaştınız.',
    'upload_limit_reached' => 'Yükleme limitinize ulaştınız.',
    'upgrade_now' => 'Şimdi Yükselt?',
    'error' => 'Hata!',
    'error_msg' => 'Bir şeyler yanlış oldu. Lütfen sonra tekrar deneyiniz!',
    'oops' => 'Hata',
    'reply' => 'Cevap',
    'write_a_reply' => 'Yorum yazın ve ENTER tuşuna basın',
    'file_is_too_big' => 'Dosya çok büyük, Maks. Yükleme boyutu',
    'ads' => 'reklâm',
    'wallet' => 'cüzdan',
    'status' => 'durum',
    'category' => 'kategori',
    'name' => 'isim',
    'results' => 'sonuçlar',
    'spent' => 'harcanmış',
    'action' => 'eylem',
    'views' => 'Görünümler',
    'clicks' => 'Tıklanma',
    'create_ad' => 'Reklam oluştur',
    'my_balance' => 'MY BALANCE',
    'replenish_my_balance' => 'Bakiyemi yenile',
    'amount' => 'miktar',
    'replenish' => 'doldurmak',
    'create_new_ad' => 'Yeni reklam oluştur',
    'title' => 'başlık',
    'description' => 'tanım',
    'select_media' => 'Medya Seç',
    'pricing' => 'Fiyatlandırma',
    'placement' => 'yerleştirme',
    'traget_audience' => 'Hedef Kitle',
    'video_ad' => 'Videolar (Biçim Video / Resim)',
    'page_ad' => 'Kenar Çubuğu (Biçim Resim)',
    'cost_click' => 'Tıklama Başı Ödeme',
    'cost_view' => 'Gösterim Başı Ödeme',
    'invalid_name' => 'Ad 5/32 arasında olmalıdır',
    'invalid_url' => 'URL geçersiz. Lütfen geçerli bir URL girin',
    'invalid_ad_title' => 'Reklam başlığı 5/100 arasında olmalıdır.',
    'invalid_videoad_media' => 'Medya dosyası geçersiz. Lütfen geçerli bir resim / video seçin',
    'invalid_pagead_media' => 'Medya dosyası geçersiz. Lütfen geçerli bir resim seçin',
    'edit_ad' => 'Reklamı düzenle',
    'by' => 'tarafından',
    'more_info' => 'Daha Fazla Bilgi',
    'monetization' => 'Para Kazanma',
    'monetization_settings' => 'Para Kazandırma Ayarları',
    'withdrawals' => 'Çekme',
    'balance' => 'Kullanılabilir bakiye',
    'min' => 'min',
    'submit_withdrawal_request' => 'Istek gönder',
    'cant_request_withdrawal' => 'Önceki istek onaylanana / reddedilene kadar istekte bulunamazsınız',
    'withdrawal_request_amount_is' => 'Bakiyeniz {{BALANCE}}, minimum para çekme isteği 50\'dir:',
    'min_withdrawal_request_amount_is' => 'Minimum para çekme talebi 50\'dir:',
    'withdrawal_request_sent' => 'Çekilme isteğiniz başarıyla gönderildi!',
    'enabled' => 'Etkin',
    'disabled' => 'engelli',
    'withdrawals_history' => 'Para Çekme Tarihi',
    'rejected' => 'reddedilen',
    'accepted' => 'kabul',
    'requested_at' => 'Adresinden istendi',
    'confirm_delete_ad' => 'Bu reklamı silmek istediğinizden emin misiniz?',
    'ad_published' => 'Reklamınız başarıyla yayınlandı',
    'ad_saved' => 'Reklamdaki değişiklikler başarıyla kaydedildi',
    'pending' => 'kadar',
    'balance_is_0' => 'Mevcut cüzdan bakiyeniz: 0, devam etmek için lütfen cüzdanınızı doldurun.',
    'top_up' => 'Ekleyin',
    'earn_mon' => 'Videolarınızdan aldığınız her bir reklam tıklaması için {{CONFIG pub_price}} {{CONFIG payment_currency}} kazanın!'
);

$categories = array(
    '1' => 'Film ve Animasyon',
'2' => 'Otomobiller ve Araçlar',
'3' => 'Müzik',
'4' => 'Hayvanlar ve Hayvanlar',
'5' => 'Spor Dalları',
'6' => 'Seyahat ve Etkinlikler',
'7' => 'kumar',
'8' => 'Kişiler ve Bloglar',
'9' => 'Komedi',
'10' => 'Eğlence',
'11' => 'Haberler ve Politika',
'12' => 'Nasıl Yapılır ve Stil',
'13' => 'Kâr amacı gütmeyenler & Aktivizm',
);

$countries_name   = array(
    '0' => 'Select Country',
    '1' => 'United States',
    '2' => 'Canada',
    '3' => 'Afghanistan',
    '4' => 'Albania',
    '5' => 'Algeria',
    '6' => 'American Samoa',
    '7' => 'Andorra',
    '8' => 'Angola',
    '9' => 'Anguilla',
    '10' => 'Antarctica',
    '11' => 'Antigua and/or Barbuda',
    '12' => 'Argentina',
    '13' => 'Armenia',
    '14' => 'Aruba',
    '15' => 'Australia',
    '16' => 'Austria',
    '17' => 'Azerbaijan',
    '18' => 'Bahamas',
    '19' => 'Bahrain',
    '20' => 'Bangladesh',
    '21' => 'Barbados',
    '22' => 'Belarus',
    '23' => 'Belgium',
    '24' => 'Belize',
    '25' => 'Benin',
    '26' => 'Bermuda',
    '27' => 'Bhutan',
    '28' => 'Bolivia',
    '29' => 'Bosnia and Herzegovina',
    '30' => 'Botswana',
    '31' => 'Bouvet Island',
    '32' => 'Brazil',
    '34' => 'Brunei Darussalam',
    '35' => 'Bulgaria',
    '36' => 'Burkina Faso',
    '37' => 'Burundi',
    '38' => 'Cambodia',
    '39' => 'Cameroon',
    '40' => 'Cape Verde',
    '41' => 'Cayman Islands',
    '42' => 'Central African Republic',
    '43' => 'Chad',
    '44' => 'Chile',
    '45' => 'China',
    '46' => 'Christmas Island',
    '47' => 'Cocos (Keeling) Islands',
    '48' => 'Colombia',
    '49' => 'Comoros',
    '50' => 'Congo',
    '51' => 'Cook Islands',
    '52' => 'Costa Rica',
    '53' => 'Croatia (Hrvatska)',
    '54' => 'Cuba',
    '55' => 'Cyprus',
    '56' => 'Czech Republic',
    '57' => 'Denmark',
    '58' => 'Djibouti',
    '59' => 'Dominica',
    '60' => 'Dominican Republic',
    '61' => 'East Timor',
    '62' => 'Ecuador',
    '63' => 'Egypt',
    '64' => 'El Salvador',
    '65' => 'Equatorial Guinea',
    '66' => 'Eritrea',
    '67' => 'Estonia',
    '68' => 'Ethiopia',
    '69' => 'Falkland Islands (Malvinas)',
    '70' => 'Faroe Islands',
    '71' => 'Fiji',
    '72' => 'Finland',
    '73' => 'France',
    '74' => 'France, Metropolitan',
    '75' => 'French Guiana',
    '76' => 'French Polynesia',
    '77' => 'French Southern Territories',
    '78' => 'Gabon',
    '79' => 'Gambia',
    '80' => 'Georgia',
    '81' => 'Germany',
    '82' => 'Ghana',
    '83' => 'Gibraltar',
    '84' => 'Greece',
    '85' => 'Greenland',
    '86' => 'Grenada',
    '87' => 'Guadeloupe',
    '88' => 'Guam',
    '89' => 'Guatemala',
    '90' => 'Guinea',
    '91' => 'Guinea-Bissau',
    '92' => 'Guyana',
    '93' => 'Haiti',
    '94' => 'Heard and Mc Donald Islands',
    '95' => 'Honduras',
    '96' => 'Hong Kong',
    '97' => 'Hungary',
    '98' => 'Iceland',
    '99' => 'India',
    '100' => 'Indonesia',
    '101' => 'Iran (Islamic Republic of)',
    '102' => 'Iraq',
    '103' => 'Ireland',
    '104' => 'Israel',
    '105' => 'Italy',
    '106' => 'Ivory Coast',
    '107' => 'Jamaica',
    '108' => 'Japan',
    '109' => 'Jordan',
    '110' => 'Kazakhstan',
    '111' => 'Kenya',
    '112' => 'Kiribati',
    '113' => 'Korea, Democratic People\'s Republic of',
    '114' => 'Korea, Republic of',
    '115' => 'Kosovo',
    '116' => 'Kuwait',
    '117' => 'Kyrgyzstan',
    '118' => 'Lao People\'s Democratic Republic',
    '119' => 'Latvia',
    '120' => 'Lebanon',
    '121' => 'Lesotho',
    '122' => 'Liberia',
    '123' => 'Libyan Arab Jamahiriya',
    '124' => 'Liechtenstein',
    '125' => 'Lithuania',
    '126' => 'Luxembourg',
    '127' => 'Macau',
    '128' => 'Macedonia',
    '129' => 'Madagascar',
    '130' => 'Malawi',
    '131' => 'Malaysia',
    '132' => 'Maldives',
    '133' => 'Mali',
    '134' => 'Malta',
    '135' => 'Marshall Islands',
    '136' => 'Martinique',
    '137' => 'Mauritania',
    '138' => 'Mauritius',
    '139' => 'Mayotte',
    '140' => 'Mexico',
    '141' => 'Micronesia, Federated States of',
    '142' => 'Moldova, Republic of',
    '143' => 'Monaco',
    '144' => 'Mongolia',
    '145' => 'Montenegro',
    '146' => 'Montserrat',
    '147' => 'Morocco',
    '148' => 'Mozambique',
    '149' => 'Myanmar',
    '150' => 'Namibia',
    '151' => 'Nauru',
    '152' => 'Nepal',
    '153' => 'Netherlands',
    '154' => 'Netherlands Antilles',
    '155' => 'New Caledonia',
    '156' => 'New Zealand',
    '157' => 'Nicaragua',
    '158' => 'Niger',
    '159' => 'Nigeria',
    '160' => 'Niue',
    '161' => 'Norfork Island',
    '162' => 'Northern Mariana Islands',
    '163' => 'Norway',
    '164' => 'Oman',
    '165' => 'Pakistan',
    '166' => 'Palau',
    '167' => 'Panama',
    '168' => 'Papua New Guinea',
    '169' => 'Paraguay',
    '170' => 'Peru',
    '171' => 'Philippines',
    '172' => 'Pitcairn',
    '173' => 'Poland',
    '174' => 'Portugal',
    '175' => 'Puerto Rico',
    '176' => 'Qatar',
    '177' => 'Reunion',
    '178' => 'Romania',
    '179' => 'Russian Federation',
    '180' => 'Rwanda',
    '181' => 'Saint Kitts and Nevis',
    '182' => 'Saint Lucia',
    '183' => 'Saint Vincent and the Grenadines',
    '184' => 'Samoa',
    '185' => 'San Marino',
    '186' => 'Sao Tome and Principe',
    '187' => 'Saudi Arabia',
    '188' => 'Senegal',
    '189' => 'Serbia',
    '190' => 'Seychelles',
    '191' => 'Sierra Leone',
    '192' => 'Singapore',
    '193' => 'Slovakia',
    '194' => 'Slovenia',
    '195' => 'Solomon Islands',
    '196' => 'Somalia',
    '197' => 'South Africa',
    '198' => 'South Georgia South Sandwich Islands',
    '199' => 'Spain',
    '200' => 'Sri Lanka',
    '201' => 'St. Helena',
    '202' => 'St. Pierre and Miquelon',
    '203' => 'Sudan',
    '204' => 'Suriname',
    '205' => 'Svalbarn and Jan Mayen Islands',
    '206' => 'Swaziland',
    '207' => 'Sweden',
    '208' => 'Switzerland',
    '209' => 'Syrian Arab Republic',
    '210' => 'Taiwan',
    '211' => 'Tajikistan',
    '212' => 'Tanzania, United Republic of',
    '213' => 'Thailand',
    '214' => 'Togo',
    '215' => 'Tokelau',
    '216' => 'Tonga',
    '217' => 'Trinidad and Tobago',
    '218' => 'Tunisia',
    '219' => 'Turkey',
    '220' => 'Turkmenistan',
    '221' => 'Turks and Caicos Islands',
    '222' => 'Tuvalu',
    '223' => 'Uganda',
    '224' => 'Ukraine',
    '225' => 'United Arab Emirates',
    '226' => 'United Kingdom',
    '227' => 'United States minor outlying islands',
    '228' => 'Uruguay',
    '229' => 'Uzbekistan',
    '230' => 'Vanuatu',
    '231' => 'Vatican City State',
    '232' => 'Venezuela',
    '233' => 'Vietnam',
    '238' => 'Yemen',
    '239' => 'Yugoslavia',
    '240' => 'Zaire',
    '241' => 'Zambia',
    '242' => 'Zimbabwe'
);
?>